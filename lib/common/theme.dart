import 'package:flutter/material.dart';
import 'package:sesc_1/common/colors.dart';

var light = LightColor();

var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: light.text,
      fontWeight: FontWeight.w700,
      fontSize: 24,
      fontFamily: "Roboto"
    ),
    titleMedium: TextStyle(
      color: light.subText,
      fontFamily: "Roboto",
      fontSize: 14,
      fontWeight: FontWeight.w500
    ),
    titleSmall: TextStyle(
        color: light.block,
        fontFamily: "Roboto",
        fontSize: 16,
        fontWeight: FontWeight.w500
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: light.accent,
      foregroundColor: light.disableAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  )
);