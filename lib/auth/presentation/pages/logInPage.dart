import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sesc_1/auth/presentation/pages/signUpPage.dart';
import 'package:sesc_1/common/colors.dart';
import 'package:sesc_1/common/theme.dart';
import 'package:sesc_1/common/widgets/customTextField.dart';

import 'forgotPasswordPage.dart';

class LogInPage extends StatefulWidget {
  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  var email = TextEditingController();

  var password = TextEditingController();

  var obscurePassword = true;

  bool rememberPassword = false;

  @override
  Widget build(BuildContext context) {
    var light = LightColor();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 83,
            ),
            Text("Создать аккаунт",
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(
              height: 8,
            ),
            Text("Завершите регистрацию чтобы начать",
                style: Theme.of(context).textTheme.titleMedium),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            CustomTextField(
                label: "Пароль",
                hint: "***********",
                controller: password,
                enableObscure: obscurePassword,),
            SizedBox(
              height: 18,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox.square(
                    dimension: 22,
                    child: Transform.scale(
                      scale: 1.2,
                      child: Checkbox(
                        side: BorderSide(
                          color: light.subText
                        ),
                        value: rememberPassword,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        onChanged: (newValue) {
                          setState(() {
                            rememberPassword = newValue!;
                          });
                        },
                      ),
                    ),
                  ),
                    const SizedBox(width: 8),
                    Text("Запомнить меня",
                        style: Theme.of(context).textTheme.titleMedium),
                ]),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => ForgotPasswordPage()));
                      },
                      child: Text("Забыли пароль?",
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(color: light.accent)),
                    ),
              ],),
            SizedBox(
              height: 14,
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  child: SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                        onPressed: () {},
                        child: Text(
                          "Зарегестрировать",
                          style: Theme.of(context).textTheme.titleSmall,
                        )),
                  ),
                ),
                SizedBox(height: 14,),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => SignUpPage()));
                  },
                  child: RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text: "У меня нет аккаунта! ",
                        style: Theme.of(context).textTheme.titleMedium),
                    TextSpan(
                        text: "Создать",
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium
                            ?.copyWith(color: light.accent))
                  ])),
                ),
                const SizedBox(
                  height: 32,
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
