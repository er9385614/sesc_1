import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sesc_1/auth/presentation/pages/logInPage.dart';
import 'package:sesc_1/auth/presentation/pages/otpVerificationPage.dart';
import 'package:sesc_1/common/colors.dart';
import 'package:sesc_1/common/theme.dart';
import 'package:sesc_1/common/widgets/customTextField.dart';

class ForgotPasswordPage extends StatefulWidget{

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  var email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var light = LightColor();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body:
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83,),
            Text("Восстановление пароля",
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 8,),
            Text("Введите свою почту",
                style: Theme.of(context).textTheme.titleMedium),
            SizedBox(
              width: double.infinity,
              child:
              CustomTextField(
                  label: "Почта",
                  hint: "***********@mail.com",
                  controller: email),
            ),
            SizedBox(height: 14,),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (_) => OTPVerificationPage()),
                                    (route) => false);
                          },
                          child:  Text("Отправить код",
                            style: Theme.of(context).textTheme.titleSmall,)),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (_) => LogInPage()));
                      },
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(text: "Вернуться",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                    color: light.accent
                                ))
                          ]
                      )),
                    ),
                    const SizedBox(height: 32,)
                  ],
                )
            )
          ],
        ),
      ),
    );
  }
}