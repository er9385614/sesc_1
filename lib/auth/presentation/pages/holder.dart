import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sesc_1/auth/domain/holderPresenter.dart';

class Holder extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child: SizedBox(
            width: double.infinity,
            child: FilledButton(
                onPressed: (){
                  pressExitButton(() => exit(0), (p0) => null);
                },
                child: Text("ВЫХОД",
                style: Theme.of(context).textTheme.titleSmall,)),
          ),
        ),
      ),
    );
  }

}