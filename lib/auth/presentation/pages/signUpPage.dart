import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sesc_1/auth/domain/logInPresenter.dart';
import 'package:sesc_1/auth/presentation/pages/holder.dart';
import 'package:sesc_1/auth/presentation/pages/logInPage.dart';
import 'package:sesc_1/common/colors.dart';
import 'package:sesc_1/common/theme.dart';
import 'package:sesc_1/common/widgets/customTextField.dart';

import '../../domain/signUpPresenter.dart';

class SignUpPage extends StatefulWidget{

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  var email = TextEditingController();
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  var obscurePassword = true;
  var obscureConfirmPassword = true;


  @override
  Widget build(BuildContext context) {
    var light = LightColor();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body:
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83,),
            Text("Создать аккаунт",
            style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 8,),
            Text("Завершите регистрацию чтобы начать",
              style: Theme.of(context).textTheme.titleMedium),
            SizedBox(
              width: double.infinity,
              child:
              CustomTextField(
                  label: "Почта",
                  hint: "***********@mail.com",
                  controller: email),
            ),
            SizedBox(
              width: double.infinity,
              child:
              CustomTextField(
                  label: "Пароль",
                  hint: "***********",
                  controller: password,
                  enableObscure: obscurePassword,),
            ),
            SizedBox(
              width: double.infinity,
              child:
              CustomTextField(
                  label: "Повторите пароль",
                  hint: "***********",
                  controller: confirmPassword,
                  enableObscure: obscureConfirmPassword),
            ),
            SizedBox(height: 14,),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            pressSignUp(
                                email.text,
                                password.text,
                                    (_) =>  Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (_) => Holder()),
                                    (route) => false),
                                    (_) => Navigator.of(context).push(
                                        MaterialPageRoute(builder: (_) => Holder())));

                          },
                          child:  Text("Зарегестрироватьcя",
                          style: Theme.of(context).textTheme.titleSmall,)),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (_) => LogInPage()));
                      },
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(text: "У меня уже есть аккаунт! ",
                          style: Theme.of(context).textTheme.titleMedium),
                          TextSpan(text: "Войти",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: light.accent
                              ))
                        ]
                      )),
                    ),
                    const SizedBox(height: 32,)
                  ],
              )
            )
          ],
        ),
      ),
    );
  }
}