import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sesc_1/auth/presentation/pages/logInPage.dart';
import 'package:sesc_1/auth/presentation/pages/otpVerificationPage.dart';
import 'package:sesc_1/auth/presentation/pages/signUpPage.dart';
import 'package:sesc_1/common/colors.dart';
import 'package:sesc_1/common/theme.dart';
import 'package:sesc_1/common/widgets/customTextField.dart';

import 'forgotPasswordPage.dart';

class NewPasswordPage extends StatefulWidget {
  @override
  State<NewPasswordPage> createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {

  var password = TextEditingController();

  var confirmPassword = TextEditingController();

  var obscurePassword = true;
  var obscureConfirmPassword = true;


  @override
  Widget build(BuildContext context) {
    var light = LightColor();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 83,
            ),
            Text("Новый пароль",
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(
              height: 8,
            ),
            Text("Введите новый пароль",
                style: Theme.of(context).textTheme.titleMedium),
            CustomTextField(
                label: "Пароль",
                hint: "***********",
                controller: password,
                enableObscure: obscurePassword,),
            CustomTextField(
                label: "Подтвердить пароль",
                hint: "***********",
                controller: confirmPassword,
                enableObscure: obscureConfirmPassword),
            SizedBox(
              height: 18,
            ),

            SizedBox(
              height: 14,
            ),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: () {
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (_) => LogInPage()));
                          },
                          child: Text(
                            "Подтвердить",
                            style: Theme.of(context).textTheme.titleSmall,
                          )),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (_) => LogInPage()));
                      },
                      child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                                text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(
                                text: "Вернуться",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium
                                    ?.copyWith(color: light.accent))
                          ])),
                    ),
                    const SizedBox(
                      height: 32,
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
