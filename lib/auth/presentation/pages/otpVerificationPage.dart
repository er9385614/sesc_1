import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pinput/pinput.dart';
import 'package:sesc_1/auth/presentation/pages/logInPage.dart';
import 'package:sesc_1/auth/presentation/pages/newPasswordPage.dart';
import 'package:sesc_1/common/colors.dart';
import 'package:sesc_1/common/theme.dart';
import 'package:sesc_1/common/widgets/customTextField.dart';

class OTPVerificationPage extends StatefulWidget{

  @override
  State<OTPVerificationPage> createState() => _OTPVerificationPageState();
}

class _OTPVerificationPageState extends State<OTPVerificationPage> {
  TextEditingController code = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var light = LightColor();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body:
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83,),
            Text("Верификация",
                style: Theme.of(context).textTheme.titleLarge),
            const SizedBox(height: 8,),
            Text("Введите 6-ти значный код из письма ",
                style: Theme.of(context).textTheme.titleMedium),
            SizedBox(height: 58,),
            Pinput(
              length: 6,
              controller: code,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: light.subText),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              focusedPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: light.subText),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              ),

            SizedBox(height: 14,),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (_) => NewPasswordPage()),
                                    (route) => false);
                          },
                          child:  Text("Сбросить пароль",
                            style: Theme.of(context).textTheme.titleSmall,)),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => LogInPage()));},
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(text: "Вернуться",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                    color: light.accent
                                ))
                          ]
                      )),
                    ),
                    const SizedBox(height: 32,)
                  ],
                )
            )
          ],
        ),
      ),
    );
  }
}