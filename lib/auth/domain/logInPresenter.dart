import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../common/utils.dart';
import '../data/models/modelAuth.dart';
import '../data/repository/supabase.dart';

late final SharedPreferences sharedPreferences;

void pressSignIn(
    String email,
    String hashPassword,
    bool isRememberMe,
    Function(AuthResponse) onResponse,
    Function(String) onError,
    ){
  ModelAuth modelAuth = ModelAuth(email: email, password: hashPassword);
  request(
      request: () async { return await logIn(modelAuth); },
      onResponse: (AuthResponse e) async {
        if (isRememberMe){
          await sharedPreferences.setString("email", email);
          await sharedPreferences.setString("hashPassword", hashPassword);
        }
        onResponse(e);
      },
      onError: onError
  );
}
