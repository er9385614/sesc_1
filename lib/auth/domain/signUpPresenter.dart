import 'package:sesc_1/auth/data/models/modelAuth.dart';
import 'package:sesc_1/auth/data/repository/supabase.dart';
import 'package:sesc_1/common/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressSignUp(String email, String password,
    Function(AuthResponse) onResponse, Function(String) onError) async {
  ModelAuth modelAuth = ModelAuth(email: email, password: password);
  request(
      request: () async {
        return await signUp(modelAuth);
      },
      onResponse: onResponse,
      onError: onError);
}
