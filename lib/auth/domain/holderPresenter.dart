import 'package:sesc_1/auth/data/repository/supabase.dart';
import 'package:sesc_1/common/utils.dart';

import 'logInPresenter.dart';

Future<void> pressExitButton(
    Function() onResponse,
    Function(String) onError
    ) async {
  await request(
      request: logOut,
      onResponse: (_) async {
        await sharedPreferences.clear();
        onResponse();
      },
      onError: onError
  );
}
